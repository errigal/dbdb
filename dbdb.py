#!/usr/bin/python
# User: Avi 
# Date: Weekends and sleepless nights.

from Tkinter import *
import ttk
import tkSimpleDialog
from os.path import *
import psutil
import os
from glob import glob
import tkMessageBox
import time
from threading import Thread
import urllib
import tarfile
import shutil
import subprocess
import getpass


def initDB(baseDir,dbname):
    user = getpass.getuser()
    args = [baseDir+'/mysql/bin/mysqld', '--basedir='+baseDir+'/mysql', '--user='+user, '--datadir='+baseDir+'/data/'+dbname, '--pid-file='+baseDir+'/pid/'+dbname+'.pid', '--initialize']
    p = subprocess.Popen(args)
    (output, err) = p.communicate()  
    p_status = p.wait()
    shutil.copy(baseDir+'/initFile.txt',baseDir+'/data/'+dbname+'/initFile.txt')
    shutil.copy(baseDir+'/my.cnf',baseDir+'/data/'+dbname+'/my.cnf')


def useDB(baseDir,dbname):
    user = getpass.getuser()
    args = [baseDir+'/mysql/bin/mysqld', '--defaults-file='+baseDir+'/data/'+dbname+'/my.cnf',
    '--basedir='+baseDir+'/mysql', '--user='+user, '--datadir='+baseDir+'/data/'+dbname, 
    '--pid-file='+baseDir+'/pid/'+dbname+'.pid', '--init-file='+baseDir+'/data/'+dbname+'/initFile.txt','&']
    param = " ".join(args)
    os.system(param)

def stopDB():
    args = ['pkill','-f','mysqld']
    param = " ".join(args)
    os.system(param)


def find_procs_by_name(name):
    "Return a list of processes matching 'name'."
    ls = []
    for p in psutil.process_iter(attrs=['name']):
        if p.info['name'] == name:
            ls.append(p.exe())
    return ls

def createDir(test):
    if not os.path.exists(test):
        try:
            os.makedirs(test)
        except:
            raise OSError("Can't create destination directory (%s)!" % (test))  

def deleteDir(test):
    if os.path.exists(test):
        try:
            shutil.rmtree(test)
        except:
            raise OSError("Can't create destination directory (%s)!" % (test)) 

def createFile(test,data):
    if not os.path.exists(test):
        try:
            file = open(test,"w") 
            file.write(data) 
            file.close() 
        except:
            raise OSError("Can't create destination file (%s)!" % (test))  


def runOnUseClick(i):
    global listbox,baseDir
    cursel = listbox.curselection()
    if len(cursel) > 0 :
        index = int(listbox.curselection()[0])
        value = listbox.get(index)
        if value != "":
            if os.path.exists(baseDir+"/data/"+value+"/ibdata1"):
                print "DB already inited."
            else:
                tprint ("DB not inited yet, initializing..","orange")
                initDB(baseDir,value)
                print "DB inited."
            tprint ("stopping existing instance (if any).","orange")
            stopDB()
            tprint ("starting instance.","orange")
            useDB(baseDir,value)
            tprint ("Please wait...","orange")
            time.sleep(3)
            reset_title("#efefef")
    else:
        tkMessageBox.showerror("Error","Select atleast one record.")

def onUseClick():
    t4 = Thread(target=runOnUseClick, args=(1,))
    t4.start()


homeDir = expanduser("~")
baseDir = homeDir+"/.dbdb"
createDir(baseDir+"/data")
createDir(baseDir+"/pid")
createFile(baseDir+"/initFile.txt","ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'ozzrules';")
createFile(baseDir+"/my.cnf","[mysqld]\nmax_allowed_packet=256M")

master = Tk()

def on_closing():
    global master
    master.destroy()
    os._exit(0)

master.geometry("230x350")
master.title("DBDB - Database of Databases - By Avi")
master.protocol("WM_DELETE_WINDOW", on_closing)

label = Label(master,text="DBDB")
label.pack()

def tprint(text, color='gray'):
    global label
    print text
    label['text'] = text
    label["bg"] = color

def reset_title(color='gray'):
    tprint("DBDB",color)

runningLabel = Label(master,text="RUNNING: NONE")
runningLabel.pack()

selectedListBoxValue = "none selected :("
def onselect(evt):
    global selectedListBoxValue
    w = evt.widget
    index = int(w.curselection()[0])
    selectedListBoxValue = w.get(index)
    print 'You selected item %d: "%s"' % (index, selectedListBoxValue)

listbox = Listbox(master)
listbox.bind('<<ListboxSelect>>', onselect)
listbox.pack()

button = Button(master,text="Use Scope", command=onUseClick)
button.pack()

def addDB():
    string = tkSimpleDialog.askstring("String", "Avi Says, Enter Name (a-z allowed.)")
    if string is None:
        return
    string = string.strip()
    if string != "" :
        createDir(baseDir+"/data/"+string)
        refresh_db_list()

def removeDB():
    global listbox
    cursel = listbox.curselection()
    if len(cursel) > 0 :
        index = int(listbox.curselection()[0])
        value = listbox.get(index)
        if value != "":
                result = tkMessageBox.askquestion("Delete", "Avi Asks: Delete and Drop All Databases in "+value+"?", icon='warning')
                if result == 'yes':
                    deleteDir(baseDir+"/data/"+value)
                    refresh_db_list()
                else:
                    print("Not Deleted Yet")
    else:
        tkMessageBox.showerror("Error","Avi Says, Select atleast one record.")

def stopAndQuit():
    tprint("Stopping..","red")
    stopDB()
    time.sleep(1)
    on_closing()



button2 = Button(master,text="Add Scope", command=addDB)
button2.pack()

button3 = Button(master,text="Remove Scope", command=removeDB)
button3.pack()

button4 = Button(master,text="Stop & Quit", command=stopAndQuit)
button4.pack()

def activate_buttons():
    global button,button2,button3
    button['state'] = "normal"
    button2['state'] = "normal"
    button3['state'] = "normal"

def deactivate_buttons():
    global button,button2,button3
    button['state'] = "disabled"
    button2['state'] = "disabled"
    button3['state'] = "disabled"


if os.listdir(baseDir+"/data") == []: 
    for item in ["ext", "cc", "atc", "other"]:
        createDir(baseDir+"/data/"+item)
        listbox.insert(END, item)
else: 
    print "data exists."  

def refresh_running_status():
        global baseDir,listbox, selectedListBoxValue
        running = False
        running_mysql = find_procs_by_name('mysqld')
        if len(running_mysql) >0 and '.dbdb' in running_mysql[0]:
            pids = os.listdir(baseDir+"/pid")
            for pid in pids:
                if ".pid" in pid:
                    runningLabel['text'] = 'Running: '+pid.replace(".pid"," ~")
                    runningLabel["bg"] = "green"
                    running = True
        if running == False:
                runningLabel['text'] = 'Running: None'
                runningLabel["bg"] = "gray"


def refresh_db_list():
        global baseDir,listbox, selectedListBoxValue
        dirs = os.listdir(baseDir+"/data")
        listbox.delete(0, END)
        i = 0
        for dirEl in dirs:
            if os.path.isdir(baseDir+"/data/"+dirEl):
                listbox.insert(END, dirEl)
                if dirEl == selectedListBoxValue:
                    listbox.selection_set(i)
                i = i+1


def fetchStatus(i):
    while True:
        refresh_running_status()
        refresh_db_list()
        time.sleep(15)

def fetch_running_status(i):
    while True:
        refresh_running_status()
        time.sleep(3)

running_mysql = find_procs_by_name('mysqld')
print running_mysql
if len(running_mysql) >0 and '.dbdb' not in running_mysql[0]:
    master.withdraw()
    tkMessageBox.showerror("Error","Avi Says, Please kill/stop the current MySQL process as it is not managed by DBDB.")
    master.destroy()
    exit()

progress = ttk.Progressbar(orient="horizontal",length=200, mode="determinate")
progress.pack()

def dlProgress(count, blockSize, totalSize):
    global progress
    progress["maximum"] = totalSize
    progress['value'] = min(count*blockSize, totalSize)


def downloadFunc(i,progress):
    global label, baseDir
    label['text'] = "Please wait ..."
    label["bg"] = "red"
    deactivate_buttons()
    url = 'http://www.mirrorservice.org/sites/ftp.mysql.com/Downloads/MySQL-5.7/mysql-5.7.30-macos10.14-x86_64.tar.gz'
    urllib.urlretrieve(url, '/tmp/mysql.tar.gz', reporthook=dlProgress)
    label['text'] = "5 secs more.."
    label["bg"] = "yellow"
    tar = tarfile.open('/tmp/mysql.tar.gz')
    tar.extractall(path=baseDir)
    tar.close()
    f = glob(os.path.join(baseDir,"mysql-*"))[0]
    os.rename(f, os.path.join(baseDir,"mysql"))
    label['text'] = "DBDB"
    label["bg"] = "green"
    activate_buttons()
    print "finished sleeping from thread %d" % i

if not os.path.exists(baseDir+"/mysql/bin"):
    t = Thread(target=downloadFunc, args=(1,progress,))
    t.start()

t2 = Thread(target=fetchStatus, args=(1,))
t2.start()


t3 = Thread(target=fetch_running_status, args=(1,))
t3.start()

master.focus_force()
mainloop()


#
